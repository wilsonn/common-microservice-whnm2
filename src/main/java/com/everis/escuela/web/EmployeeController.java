package com.everis.escuela.web;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.everis.escuela.exception.ResourceNotFoundException;
import com.everis.escuela.models.Employee;
import com.everis.escuela.service.EmployeeService;

@RestController
@RequestMapping("/api/business/v1")
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;
	
	@PostMapping("/employees")
	public String createEmployee(@RequestBody Employee employee){
		
		employeeService.saveEmployee(employee);
		return "Se guardo exitosamente";
	}
	
	@PutMapping("/employees/{dni}")
	public Employee updateEmployed(@PathVariable(value="dni") Long dni, @RequestBody Employee employee) throws ResourceNotFoundException{
		Employee emp = new Employee();
		emp = employeeService.actualizarEmployee(dni, employee);
		return emp;
	}
	
	@GetMapping("/employees/{dni}")
	public Employee buscarEmpleado(@PathVariable(value="dni") String dni)  throws ResourceNotFoundException{
		Employee emp = new Employee();
		emp = employeeService.buscarEmployee(dni);
		return emp;
	}
	
	@GetMapping("/employees2/{dni}")
	public Employee buscarEmpleadoDNI(@PathVariable(value="dni") String dni)  throws ResourceNotFoundException{
		return employeeService.buscarEmployeeDNI(dni);
	}
	
	@DeleteMapping("/employees/{id}")
	public Map<String, Boolean> deleteEmployee(@PathVariable(value = "id") Long employeeId)
			throws ResourceNotFoundException {
		return employeeService.deleteEmployee(employeeId);
	}
	
	@GetMapping("/employees")
	public List<Employee> getAllEmployees() {
		return employeeService.getAllEmployees();
	}
	
	@GetMapping("/employees/city/{city}")
	public List<Employee> getListEmployeeByCity(@PathVariable(value = "city") String city) {
		return employeeService.getListEmployeeByCity(city);
	}

}
