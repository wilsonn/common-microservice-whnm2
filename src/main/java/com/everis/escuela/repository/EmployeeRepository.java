package com.everis.escuela.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.everis.escuela.models.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long>{
	Optional<Employee> findByDocumentNumber(String documentNumber);
	
	public Optional<Employee> findById(Long id);
	
	@Query("SELECT e FROM Employee e WHERE e.city = :city")
	public List<Employee> getListEmployeeByCity(@Param("city") String city);


}
