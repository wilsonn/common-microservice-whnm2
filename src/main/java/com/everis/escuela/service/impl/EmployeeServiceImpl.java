package com.everis.escuela.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import com.everis.escuela.exception.ResourceNotFoundException;
import com.everis.escuela.models.Employee;
import com.everis.escuela.repository.EmployeeRepository;
import com.everis.escuela.service.EmployeeService;

@Service
public class EmployeeServiceImpl implements EmployeeService{

	@Autowired
	private EmployeeRepository employeeRepository;

	@Override
	public void saveEmployee(Employee employee) {
		employeeRepository.save(employee);
	}

	@Override
	public Employee actualizarEmployee(Long id, Employee employeeModified) throws ResourceNotFoundException{	
		return employeeRepository.findById(id).map(employee -> {
			employee.setEmail(employeeModified.getEmail());
			employee.setLastName(employeeModified.getLastName());
			employee.setFirstName(employeeModified.getFirstName());
			employee.setDocumentNumber(employeeModified.getDocumentNumber());
			return employeeRepository.save(employee);
		}).orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + id));

	}

	@Override
	public Employee buscarEmployee(String dni) throws ResourceNotFoundException{
		return employeeRepository.findByDocumentNumber(dni).orElseThrow(
				() -> new ResourceNotFoundException("Employee not found for this documentNumber :: " + dni));

	}

	@Override
	public Employee buscarEmployeeDNI(String dni) throws ResourceNotFoundException{
		return employeeRepository.findByDocumentNumber(dni).orElseThrow(
				() -> new ResourceNotFoundException("Employee not found for this documentNumber :: " + dni));
	}	
	
	@Override
	public Map<String, Boolean> deleteEmployee(@PathVariable(value = "id") Long employeeId)
			throws ResourceNotFoundException {
		Map<String, Boolean> response = new HashMap<>();

		return employeeRepository.findById(employeeId).map(employee -> {
			employeeRepository.delete(employee);
			response.put("deleted", Boolean.TRUE);
			return response;
		}).orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + employeeId));
	}

	@Override
	public List<Employee> getAllEmployees() {
		return employeeRepository.findAll();
	}

	@Override
	public List<Employee> getListEmployeeByCity(String city) {
		return employeeRepository.getListEmployeeByCity(city);
	}

	

}
