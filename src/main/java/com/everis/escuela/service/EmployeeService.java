package com.everis.escuela.service;

import java.util.List;
import java.util.Map;

import com.everis.escuela.exception.ResourceNotFoundException;
import com.everis.escuela.models.Employee;

public interface EmployeeService {

	public void saveEmployee(Employee employee);
	public Employee actualizarEmployee(Long id, Employee employee) throws ResourceNotFoundException;
	public Employee buscarEmployee(String dni) throws ResourceNotFoundException;
	public Employee buscarEmployeeDNI(String dni) throws ResourceNotFoundException;
    public Map<String, Boolean> deleteEmployee(Long employeeId) throws ResourceNotFoundException;
	public List<Employee> getAllEmployees();
	public List<Employee> getListEmployeeByCity(String city);

}
